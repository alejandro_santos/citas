# Citas

<img src="./screenshot.png" style="width:80%" />

Imprime una cita al azar extraída de un archivo de texto al ejecutar el script; por ejemplo, en el .profile al abrir una terminal.

Funciona en Linux, OS X, Termux y RaspberryOS (en Windows, NO).

## Requerimientos

`python3`, `pip`, `numpy`, `textwrap`

## Instalación

+ Instalar requerimientos, si es necesario<br/><br/>
  
+ Clonar el repositorio:<br/><br/>
  `cd /ruta/`<br/>
  `git clone https://gitlab.com/alejandro_santos/citas.git` 

+ El archivo de citas, debe tener una cita por línea y el formato:<br/><br/>
  `<autor>#<cita>`<br><br>
  
## Uso

  `python3 /ruta/citas/citas.php /ruta_al_archivo_de_citas/archivo_citas.txt` 

Para que se ejecute al iniciar la terminal, añadir en el `~/.profile` (Linux) o 
`~/.bash_profile` (OS X) la línea:<br/><br/>
  `python3 /ruta/citas/citas.php /ruta_al_archivo_de_citas/archivo_citas.txt` 
