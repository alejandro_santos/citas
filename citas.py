import os
import sys
from random import randrange
import textwrap

def imprimir ( str_linea, int_ancho ) :

	fragmentos = str_linea.split ( "#", 1 );

	cita = get_cita ( fragmentos[1], int_ancho ) 
	autor = get_autor ( fragmentos[0], int_ancho )

	print ()

	for a in cita :
		print ( a )
	
	print ()

	for a in autor :
		print ( a )
	print ()

def get_cita ( cita_raw, ancho ) :
	return textwrap.wrap ( "«" + cita_raw.strip() + "»" , width=ancho-1 )
	
def get_autor ( autor_raw, ancho ) :
	return textwrap.wrap ( autor_raw.strip(), width=ancho-1 )

def main () :
	str_archivo = sys.argv[1]

	file = open ( str_archivo, "r" )
	arr_lineas = file.readlines ()
	file.close()   

	num_linea = randrange(len(arr_lineas))

	tup_terminal_size = os.get_terminal_size ()

	imprimir ( arr_lineas[num_linea], tup_terminal_size [0] )

if __name__ == "__main__" :
	
	main()
